/*
  @file
  logging.c

  @author
  Hardik Madaan

  @created on
  11/25/2016

  @brief
  This file has the function for logging
  
  @details
  This file has the function for logging
  
*/

/*==============================================================================
                              REVISION HISTORY
==============================================================================*/


/*==============================================================================
                                INCLUDE FILES
==============================================================================*/

#include "stdarg.h"
#include "stdio.h"
#include "logging.h"

/*==============================================================================
                             EXTERNAL DECLARATIONS
==============================================================================*/

logging_sm_s logging_sm;

/*==============================================================================
                      LOCAL DEFINITIONS AND TYPES : MACROS
==============================================================================*/

/*==============================================================================
                      LOCAL DEFINITIONS AND TYPES : ENUMS
==============================================================================*/

/*----------------------------------------------------------------------------*/
/* Function : void CRITICAL_MSG ( const char *pcString, ...) */
/*----------------------------------------------------------------------------*/
/*!
  @ brief
  This function is used to send all of its output to UART

  @ details
  This function is used to send all of its output to UART
  
  @ pre-requirements

  @ param input string(the logging format string) const uint8 *pcString
    .... optional arguments, depending upon the content of the format string  

  @ return void
  
*/

rt sendlog(char * format,...)
{ 
  /* Local variables */
  rt status = RT_SUCCESS;
  char *logstring = NULL;
  va_list arg;           //CRITICAL_MSG's arguments
  va_start( arg , format );

  unsigned int i = 0;
  char c;
  int j = 0;
  char *s = NULL;
  float f = 0.0;
  char buffer[16];

  /* Validation */
  if( NULL == format )
  {
    status = COMMON_RETURN_FAILURE;
  }

  /* Algorithm */
  if(RT_SUCCESS == status)
  {          
    for(logstring = format; *logstring != '\0'; logstring++) 
    { 
      while( TRUE )
      { 
        logging_device_send_byte(*(logstring));
        logstring++;

        if( '\0' == *(logstring) || '%' == *(logstring) )
          break;
      } 


      if('\0' == *logstring)
      {
        break;
      }

      logstring++;

      //Fetching and executing arguments
      switch(*logstring) 
      { 
        case 'c': 
          c = va_arg(arg, char); //Fetch char argument
          logging_device_send_string(&c);
        break; 

        case 'd':
          j = va_arg(arg, int); //Fetch Decimal/Integer argument
          if(j<0) {j = -j;}
          sprintf(buffer,"%d",j);
          logging_device_send_string(buffer);
        break; 

        case '%':
          logging_device_send_string("%");
        break; 

        case 's':
          s = va_arg(arg, char *); //Fetch string
          logging_device_send_string(s);
        break; 

        case 'p':
        case 'X':
        case 'x': 
          i = va_arg(arg, unsigned int); //Fetch Hexadecimal representation
          logging_device_send_string("0x");
					#warning "ADD "-u _INFO_MSG_float" to linker flags"
          sprintf(buffer,"%x",i);
          logging_device_send_string(buffer);
        break;
       
        case 'f':
          f = va_arg(arg, double); //Fetch Float representation
          sprintf(buffer,"%f",f);
          logging_device_send_string(buffer);
        break;
        
        default:

        break;
      }
    }
    //Closing argument list to necessary clean-up
    va_end(arg);
  }

  return (status);
} 
 
/*----------------------------------------------------------------------------*/
/* Function : char *convert ( unsigned int num, int base) */
/*----------------------------------------------------------------------------*/
/*!
  @ brief
  This function is used to perform  conversions into decimal,hex,etc format

  @ details
  This function is used to perform  conversions into decimal,hex,etc format
  
  @ pre-requirements

  @ param unsigned int, int
  @ return char*
  
*/

char * convert(unsigned int num, int base) 
{
  /* Local variables */
  static char Representation[]= "0123456789ABCDEF";
  static char buffer[50]; 
  char *ptr= NULL; 
  ptr = &buffer[49]; 
  *ptr = '\0'; 

  /* Algorithm */
  do 
  { 
    *--ptr = Representation[num%base]; 
    num /= base; 
  }while(num != 0);
  return(ptr); 
}

/*----------------------------------------------------------------------------*/
/* Function : logging_get_milliseconds_count( void ) */
/*----------------------------------------------------------------------------*/
/*!
  @ brief
  This function is used to get the logging count in milliseconds

  @ details
  This function is used to get the logging count in milliseconds
  
  @ pre-requirements

  @ param unsigned int, int
  @ return char*
  
*/

milliseconds_t logging_get_milliseconds_count( void )
{
  /* Local Variables */
  
  /* Algorithm  */
  return ( (milliseconds_t) HAL_GetTick());
}/*logging_get_miliseconds_count */

/*----------------------------------------------------------------------------*/
/* Function : logging_init( void ) */
/*----------------------------------------------------------------------------*/
/*!
  @ brief
  This function is used to initialize the logging flags

  @ details
  This function is used to initialize the logging flags
  
  @ pre-requirements

  @ param 
  
  @ return 
  
*/

rt logging_init(logging_config_s logging_config)
{
  /* Local Variables */
  rt status = RT_SUCCESS;

  /* Validation */
  if(LOGGING_STATE_INITIALIZED == logging_sm.state)
  {
    status = RT_INVALID_SEQ;
  }

  /* Algorithm  */
  if(RT_SUCCESS == status)
  {
    logging_sm.logging_levels.levels_u.levels_s.level_debug       = logging_config.logging_level_state[LOGGING_LEVEL_DEBUG];
    logging_sm.logging_levels.levels_u.levels_s.level_info        = logging_config.logging_level_state[LOGGING_LEVEL_INFO];
    logging_sm.logging_levels.levels_u.levels_s.level_error       = logging_config.logging_level_state[LOGGING_LEVEL_ERROR];
    logging_sm.logging_levels.levels_u.levels_s.level_critical    = logging_config.logging_level_state[LOGGING_LEVEL_CRITICAL];
    logging_sm.logging_levels.levels_u.levels_s.level_fatal       = logging_config.logging_level_state[LOGGING_LEVEL_FATAL];

    logging_sm.logging_levels.levels_u.levels_s.log_file          = logging_config.logging_info_state[LOGGING_INFO_FILE];
    logging_sm.logging_levels.levels_u.levels_s.log_function      = logging_config.logging_info_state[LOGGING_INFO_FUNCTION];
    logging_sm.logging_levels.levels_u.levels_s.log_line          = logging_config.logging_info_state[LOGGING_INFO_LINE];
    logging_sm.logging_levels.levels_u.levels_s.log_milliseconds  = logging_config.logging_info_state[LOGGING_INFO_MILLISECONDS];

    if (NULL != logging_config.logging_device_init_fp)
    {
      logging_sm.logging_device_init_fp = logging_config.logging_device_init_fp;

    	(*logging_sm.logging_device_init_fp)();
    }

    if(NULL != logging_config.logging_device)
    {
      logging_sm.logging_device = logging_config.logging_device;
    }

    if(NULL != logging_config.send_log_fp)
    {
      logging_sm.send_log_fp = logging_config.send_log_fp;
    }
  }

  if(RT_SUCCESS == status)
  {
    logging_sm.state = LOGGING_STATE_INITIALIZED;
		#ifdef DEBUG_EN    
			INFO_MSG("\n\n******Logging Init Success******");
		#endif /* DEBUG_EN */ 
  }

  return (status);

}/*logging_init */

/*----------------------------------------------------------------------------*/
/* Function : logging_device_send_string( void ) */
/*----------------------------------------------------------------------------*/
/*!
  @ brief
  This function is used to send string over logging_device.

  @ details
  This function is used to send string over logging_device for printing debugging messages.
  
  @ pre-requirements

  @ param uint8 *
  @ return status
  
*/
rt logging_device_send_string( char * string_ptr)
{
  /* Local Variables */
  rt status = RT_SUCCESS;

  /* Validation */
  if(LOGGING_STATE_INITIALIZED != logging_sm.state)
  {
    status = RT_INVALID_SEQ;
  }

  /* Algorithm */
  if(RT_SUCCESS == status)
  {
    while('\0' != * string_ptr)
    {
      status = logging_device_send_byte((uint8_t) * string_ptr);

      if(RT_SUCCESS == status)
      {
        string_ptr += 1U;
      }

      else
      {
        status = RT_FAILURE;
        break;
      }
    }
  }

  return status;
}

/*----------------------------------------------------------------------------*/
/* Function : logging_device_send_bye( void ) */
/*----------------------------------------------------------------------------*/
/*!
  @ brief
  This function is used to send byte over logging_device.

  @ details
  This function is used to send string over logging_device
  and this function is further used by uart_send_string
  to send strings.
  
  @ pre-requirements

  @ param uint8
  @ return status
  
*/
rt logging_device_send_byte( uint8_t data_byte)
{
  /* Local Variables */
  rt status = RT_SUCCESS;

  /* Validation */
  if(LOGGING_STATE_INITIALIZED != logging_sm.state)
  {
    status = RT_INVALID_SEQ;
  }

  /* Algorithm */
  if(RT_SUCCESS == status)
  {
  	#if defined !(USE_STD_IN_OUT)
  	if(NULL != logging_sm.send_log_fp)
  	{
	    (*logging_sm.send_log_fp)( logging_sm.logging_device, &data_byte, 1U, 100U );		
  	}
  	#else
  		printf("%c", data_byte);
    #endif /* USE_STD_IN_OUT */
  }

  return status;
}
