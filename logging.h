/*
 * project: breadbutter, generic
 * brief: printf style messages.
 * tags: standard return types, macros, 
 * Copyright (c) 2017 - 2025.
 * All Rights Reserved.
 * 
 * creator: Hardik Madaan
 * alter-ego: Brian doofus 
 * mail: hardik.mad@gmail.com
 * web: www.tachymoron.wordpress.com
 * git: https://bitbucket.org/bacon_toast/
 *
 */

#ifndef _LOGGING_H_
#define _LOGGING_H_

#ifdef __cplusplus
extern "C" {
#endif

/*=============================================================================
																	File-Includes
=============================================================================*/
#include "stdint.h"
#include "main.h"
#include "stdio.h"
#include "../cstm_std/cstm_std_types.h"
 
#define USE_STD_IN_OUT
	
#ifdef STM32L476xx
	#include "stm32l4xx_hal.h"
#endif

/*=============================================================================
														  		MACROs-&-ENUMs
=============================================================================*/
/*----------------------------------------------------------------------------*/
/*!@brief Macros to be define the DEBUG logging Macros */
#define DEBUG_MSG(fmt, args...) \
do { \
if ( LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.level_debug ) { \
  sendlog("[ ");  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_file)  \
  {  \
    sendlog(" | %s", __FILE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_function)  \
  {  \
    sendlog(" | %s", __FUNCTION__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_line)  \
  {  \
    sendlog(" | %s", __LINE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_milliseconds)  \
  {  \
      sendlog(" | %d", logging_get_milliseconds_count());  \
  }  \
  sendlog("]");  \
  sendlog("\nDEBUG_MSG : " fmt, ## args); } \
} while (0)

/*----------------------------------------------------------------------------*/
/*!@brief Macros to be define the INFO logging Macros */
#define INFO_MSG(fmt, args...) \
do { \
if ( LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.level_info ) { \
  sendlog("[ ");  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_file)  \
  {  \
    sendlog(" | %s", __FILE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_function)  \
  {  \
    sendlog(" | %s", __FUNCTION__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_line)  \
  {  \
    sendlog(" | %s", __LINE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_milliseconds)  \
  {  \
      sendlog(" | %d", logging_get_milliseconds_count());  \
  }  \
  sendlog("]");  \
  sendlog("\nINFO_MSG : " fmt, ## args); } \
} while (0)

/*----------------------------------------------------------------------------*/
/*!@brief Macros to be define the CRITICAL logging Macros */
#define CRITICAL_MSG(fmt, args...) \
do { \
if ( LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.level_critical ) { \
  sendlog("[ ");  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_file)  \
  {  \
    sendlog(" | %s", __FILE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_function)  \
  {  \
    sendlog(" | %s", __FUNCTION__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_line)  \
  {  \
    sendlog(" | %s", __LINE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_milliseconds)  \
  {  \
      sendlog(" | %d", logging_get_milliseconds_count());  \
  }  \
  sendlog("]");  \
  sendlog("\nCRITICAL_MSG : " fmt, ## args); } \
  logging_sm->err_counter++;\
} while (0)

/*----------------------------------------------------------------------------*/
/*!@brief Macros to be define the ERROR logging Macros */
#define ERROR_MSG(fmt, args...) \
do { \
if ( LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.level_error ) { \
  sendlog("[ ");  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_file)  \
  {  \
    sendlog(" | %s", __FILE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_function)  \
  {  \
    sendlog(" | %s", __FUNCTION__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_line)  \
  {  \
    sendlog(" | %s", __LINE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_milliseconds)  \
  {  \
      sendlog(" | %d", logging_get_milliseconds_count());  \
  }  \
  sendlog("]");  \
  sendlog("\nERROR_MSG : " fmt, ## args); } \
  logging_sm->err_counter++;\
} while (0)

/*----------------------------------------------------------------------------*/
/*!@brief Macros to be define the DEBUG logging Macros */
#define FATAL_MSG(fmt, args...) \
do { \
if ( LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.level_fatal ) { \
  sendlog("[ ");  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_file)  \
  {  \
    sendlog(" | %s", __FILE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_function)  \
  {  \
    sendlog(" | %s", __FUNCTION__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_line)  \
  {  \
    sendlog(" | %s", __LINE__);  \
  }  \
  if(LOGGING_STATUS_ENABLED == logging_sm.logging_levels.levels_u.levels_s.log_milliseconds)  \
  {  \
      sendlog(" | %d", logging_get_milliseconds_count());  \
  }  \
  sendlog("]");  \
  sendlog("\nFATAL_MSG : " fmt, ## args); } \
  logging_sm->err_counter++;\
} while (0)

/*----------------------------------------------------------------------------*/
/*!@brief enum for the Logging State */
typedef enum
{
  LOGGING_STATE_DANGLING            = 0U,
  LOGGING_STATE_INITIALIZED,

  LOGGING_STATE_INVALID,
  LOGGING_STATE_NUM                 = LOGGING_STATE_INVALID

}logging_state_e;

/*----------------------------------------------------------------------------*/
/*!@brief enum for the different types of Logging Messages */
typedef enum
{
  LOGGING_LEVEL_DEBUG             = 0U,
  LOGGING_LEVEL_INFO,
  LOGGING_LEVEL_ERROR,
  LOGGING_LEVEL_CRITICAL,
  LOGGING_LEVEL_FATAL,
  
  LOGGING_LEVEL_INVALID,
  LOGGING_LEVEL_NUM               =  LOGGING_LEVEL_INVALID
  
}logging_level_e;

typedef enum
{
  LOGGING_INFO_FILE        = 0U,
  LOGGING_INFO_FUNCTION,
  LOGGING_INFO_LINE,
  LOGGING_INFO_MILLISECONDS,

  LOGGING_INFO_INVALID,
  LOGGING_INFO_NUM        = LOGGING_INFO_INVALID

}logging_info_e;

typedef enum
{
  LOGGING_STATUS_DISABLED   = 0U,
  LOGGING_STATUS_ENABLED    = 1U,

  LOGGING_STATUS_INVALID,
  LOGGING_STATUS_NUM        = LOGGING_STATUS_INVALID

}logging_status_e;

/*=============================================================================
							 									Type-Definitions
=============================================================================*/
/* Typedefinition for Milliseconds */
typedef uint32_t milliseconds_t;

/* Loggine device init function pointer typedef */
typedef void (*logging_device_init_fp_t) (void);

#ifdef USE_HAL_DRIVER
  typedef uint8_t (*send_log_fp_t) (UART_HandleTypeDef  *, uint8_t *, uint16_t, uint32_t);
#endif /* USE_HAL_DRIVER */

/* Logging Config Struct */
typedef struct
{
	/* Logging Level Config */
	logging_status_e logging_level_state[LOGGING_LEVEL_NUM];
	/* Logging Info config */
	logging_status_e logging_info_state[LOGGING_INFO_NUM];

	#if defined STM32L476xx && defined USE_HAL_DRIVER
	  /* UART DEVICE */
	  UART_HandleTypeDef  * logging_device;

	  /* Function pointer to send log */
	  send_log_fp_t  send_log_fp;
	#else 
	  // use printf
	#endif /* USE_HAL_DRIVER */

  // function pointer to init UART device or sd card
logging_device_init_fp_t logging_device_init_fp;

}logging_config_s;

/*----------------------------------------------------------------------------*/
/*!@brief struct for the different flags to enable/disable particular kind of message */
typedef struct
{
  union
  {
    uint32_t logging_level_flags;

    struct
    { 
      uint32_t level_debug      : 1;
      uint32_t level_info       : 1;
      uint32_t level_error      : 1;
      uint32_t level_critical   : 1;
      uint32_t level_fatal      : 1;

      uint32_t log_file         : 1;
      uint32_t log_function     : 1;
      uint32_t log_line         : 1;
      uint32_t log_milliseconds : 1;

      uint32_t reserved_flag_09 : 1;
      uint32_t reserved_flag_10 : 1;
      uint32_t reserved_flag_11 : 1;
      uint32_t reserved_flag_12 : 1;
      uint32_t reserved_flag_13 : 1;
      uint32_t reserved_flag_14 : 1;
      uint32_t reserved_flag_15 : 1;
      uint32_t reserved_flag_16 : 1;
      uint32_t reserved_flag_17 : 1;
      uint32_t reserved_flag_18 : 1;
      uint32_t reserved_flag_19 : 1;
      uint32_t reserved_flag_20 : 1;
      uint32_t reserved_flag_21 : 1;
      uint32_t reserved_flag_22 : 1;
      uint32_t reserved_flag_23 : 1;
      uint32_t reserved_flag_24 : 1;
      uint32_t reserved_flag_25 : 1;
      uint32_t reserved_flag_26 : 1;
      uint32_t reserved_flag_27 : 1;
      uint32_t reserved_flag_28 : 1;
      uint32_t reserved_flag_29 : 1;
      uint32_t reserved_flag_30 : 1;
      uint32_t reserved_flag_31 : 1;

    }levels_s;
    
  }levels_u;
  
}logging_levels_s;

/*----------------------------------------------------------------------------*/
/*!@brief struct for the logging sm */
typedef struct
{
  /* Logging State */
  logging_state_e state;

  /* Logging Levels */
  logging_levels_s logging_levels;

	#if defined STM32L476xx && defined USE_HAL_DRIVER
	  /* UART DEVICE */
	  UART_HandleTypeDef  * logging_device;
	  /* Function pointer to send log */
	  send_log_fp_t  send_log_fp;
	#endif /* USE_HAL_DRIVER */

  // function pointer to init UART device or sd card
	logging_device_init_fp_t logging_device_init_fp;

	// Error counter
	uint32_t err_counter;
    
}logging_sm_s;

/*=============================================================================
							 							Extern-Declarations
=============================================================================*/
extern logging_sm_s logging_sm;

/*=============================================================================
							 						 Function-Declarations
=============================================================================*/
rt logging_init();
milliseconds_t logging_get_milliseconds_count( void );
//Our CRITICAL_MSG function
rt sendlog(char* format,...);
//Convert integer number into octal, hex, etc.
char * convert(unsigned int num, int base);
rt logging_device_send_byte( uint8_t data_byte);
rt logging_device_send_string( char * string_p);

#ifdef __cplusplus

} // extern "C"
#endif

#endif /* _LOGGING_H_ */
